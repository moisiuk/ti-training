import java.util.ArrayList;

public class InsertionSort {

    public static void insertIntoSort(ArrayList<Integer> arrayToSort) {
        int temp;
        int j;
        for (int i = 0; i < arrayToSort.size() - 1; i++) {
            if (arrayToSort.get(i) > arrayToSort.get(i + 1)) {
                temp = arrayToSort.get(i + 1);
                arrayToSort.set(i + 1, arrayToSort.get(i));
                j = i;
                while (j > 0 && temp < arrayToSort.get(j - 1)) {
                    arrayToSort.set(j, arrayToSort.get(j - 1));
                    j--;
                }
                arrayToSort.set(j, temp);
            }
        }
    }
}