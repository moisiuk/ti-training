public class Basket implements Comparable<Basket>{
    private Color color;

    Basket(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return color.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Basket basket = (Basket) o;

        return color == basket.color;
    }

    @Override
    public int hashCode() {
        return color != null ? color.hashCode() : 0;
    }

    @Override
    public int compareTo(Basket o) {
        return this.color.compareTo(o.color);
    }
}