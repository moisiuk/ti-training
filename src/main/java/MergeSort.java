public class MergeSort {
    private int[] array;
    private int[] tempArray;

    public void sort(int inputArray[]) {
        this.array = inputArray;
        int length = inputArray.length;
        this.tempArray = new int[length];
        doMergeSort(0, length - 1);
    }

    private void doMergeSort(int lowerIndex, int higherIndex) {
        if (lowerIndex < higherIndex) {
            int middle = (lowerIndex + higherIndex) / 2;
            doMergeSort(lowerIndex, middle);
            doMergeSort(middle + 1, higherIndex);
            mergeParts(lowerIndex, middle, higherIndex);
        }
    }

    private void mergeParts(int bottomIndex, int middleIndex, int topIndex) {
        for (int i = bottomIndex; i <= topIndex; i++) {
            tempArray[i] = array[i];
        }
        int i = bottomIndex;
        int j = middleIndex + 1;
        int k = bottomIndex;
        while (i <= middleIndex && j <= topIndex) {
            if (tempArray[i] <= tempArray[j]) {
                array[k] = tempArray[i];
                i++;
            } else {
                array[k] = tempArray[j];
                j++;
            }
            k++;
        }
        while (i <= middleIndex) {
            array[k] = tempArray[i];
            k++;
            i++;
        }
    }
}