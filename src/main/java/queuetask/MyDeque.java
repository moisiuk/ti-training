package queuetask;

public class MyDeque<T> {
    private MyStack<T> inbox = new MyStack<>();
    private MyStack<T> outbox = new MyStack<>();

    public void add(T value) {
        inbox.push(value);
    }

    public T take() {
        if (outbox.isEmpty()) {
            int inboxSize = inbox.size();
            for (int i = 0; i < inboxSize; i++) {
                outbox.push(inbox.pop());
            }
        }
        return outbox.pop();
    }

    public void addFirst(T value) {
        outbox.push(value);
    }

    public T takeLast() {
        if (inbox.isEmpty()) {
            int outboxSize = outbox.size();
            for (int i = 0; i < outboxSize; i++) {
                inbox.push(outbox.pop());
            }
        }
        return inbox.pop();
    }

    public int size() {
        return inbox.size() + outbox.size();
    }
}
