package queuetask;

import java.util.NoSuchElementException;

public class MyStack<T> {
    private Node first = null;
    private int size = 0;

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return size;
    }

    public void push(T value) {
        Node oldfirst = first;
        first = new Node();
        first.value = value;
        first.next = oldfirst;
        size++;
    }

    public T pop() {
        if (isEmpty()) throw new NoSuchElementException("Stack underflow");
        T value = first.value;
        first = first.next;
        size--;
        return value;
    }

    public T peek() {
        return isEmpty() ? null : first.value;
    }

    private class Node {
        private T value;
        private Node next;
    }
}
