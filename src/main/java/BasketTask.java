public class BasketTask {

    public void sort(Basket[] arrayToSort) {
        Basket temp;
        for (int i = 0; i < arrayToSort.length - 1; i++) {
            for (int j = arrayToSort.length - 1; j > i; j--) {
                if (arrayToSort[j - 1].compareTo(arrayToSort[j]) > 0) {
                    temp = arrayToSort[j - 1];
                    arrayToSort[j - 1] = arrayToSort[j];
                    arrayToSort[j] = temp;
                }
            }
        }
    }
}

