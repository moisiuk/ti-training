import queuetask.MyStack;

import java.util.HashMap;
import java.util.Map;

public class Brackets {
    public boolean ifBracketsAreCorrect(String stringWithBrackets) {

        MyStack<Character> stack = new MyStack<>();
        Map<Character, Character> bracketMap = new HashMap<>();
        bracketMap.put('(', ')');
        bracketMap.put('{', '}');
        bracketMap.put('[', ']');

        for (Character symbol : stringWithBrackets.toCharArray()) {
            if (bracketMap.containsKey(symbol)) stack.push(symbol);

            if (bracketMap.containsValue(symbol)) {
                if (symbol == bracketMap.get(stack.peek())) {
                    stack.pop();
                } else return false;
            }
        }
        return stack.isEmpty();
    }
}
