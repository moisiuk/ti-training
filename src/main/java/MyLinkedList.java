public class MyLinkedList<T> {
    private Node firstElement = null;
    private int size = 0;

    public void insert(T value) {
        Node oldFirst = firstElement;
        firstElement = new Node();
        firstElement.value = value;
        firstElement.next = oldFirst;
        size++;
    }

    public T pop(){
        if (firstElement == null) {
            return null;
        }
        T result = firstElement.value;
        firstElement = firstElement.next;
        size--;
        return result;
    }

    private class Node {
        T value;
        Node next;
    }

    public int size(){
        return size;
    }
}
