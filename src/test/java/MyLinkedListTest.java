import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class MyLinkedListTest {

    @Test
    public void listSizeTest() {
        MyLinkedList<String> list = new MyLinkedList<>();
        list.insert("ss");
        list.insert("aa");
        list.insert("ff");
        assertThat(list.size(), is(3));
    }

    @Test
    public void testInsertandPop() {
        MyLinkedList<String> list = new MyLinkedList<>();
        list.insert("ss");
        list.insert("aa");
        assertThat(list.pop(), is("aa"));
        assertThat(list.pop(), is("ss"));
        assertThat(list.size(), is(0));
    }
}