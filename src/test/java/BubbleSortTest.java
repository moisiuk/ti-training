import org.junit.Test;

import static org.junit.Assert.*;

public class BubbleSortTest {
    private BubbleSort bubbleSort = new BubbleSort();

    @Test
    public void testBubbleSort() {
        int[] originalArray = {2, 3, 6, 3, 9, 9, 10, 0};
        bubbleSort.sort(originalArray);
        int[] expectedArray = {0, 2, 3, 3, 6, 9, 9, 10};
        assertArrayEquals(expectedArray, originalArray);
    }
}