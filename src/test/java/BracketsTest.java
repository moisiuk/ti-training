import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class BracketsTest {

    @Test
    public void testIfBracketsAreCorrect(){

        String correct1 = "({[fff]})";
        String correct2 = "(5435[{fff[fff]}])";
        String incorrect1 = "(({)})";
        String incorrect2 = "[(dd({ddd)}d)]";
        String incorrect3 = "]()";
        String incorrect4 = "()]";
        String incorrect5 = "{]}";

        Brackets brackets = new Brackets();

        assertTrue(brackets.ifBracketsAreCorrect(correct1));
        assertTrue(brackets.ifBracketsAreCorrect(correct2));
        assertFalse(brackets.ifBracketsAreCorrect(incorrect1));
        assertFalse(brackets.ifBracketsAreCorrect(incorrect2));
        assertFalse(brackets.ifBracketsAreCorrect(incorrect3));
        assertFalse(brackets.ifBracketsAreCorrect(incorrect4));
        assertFalse(brackets.ifBracketsAreCorrect(incorrect5));
    }
}