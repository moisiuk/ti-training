package queuetask;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class MyStackTest {

    @Test
    public void testStack() {
        MyStack<String> stack = new MyStack<>();
        stack.push("one");
        stack.push("two");
        stack.push("three");

        String[] expected = {"three", "two", "one"};
        String[] actual = new String[3];
        actual[0] = stack.pop();
        actual[1] = stack.pop();
        actual[2] = stack.pop();

        assertThat(actual, is(expected));
        assertThat(stack.size(), is(0));
    }
}