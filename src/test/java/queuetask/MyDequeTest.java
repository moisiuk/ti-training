package queuetask;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class MyDequeTest {

    @Test
    public void testAddandTake(){
        MyDeque<String> deque = new MyDeque<>();

        deque.add("aa");
        deque.add("bb");
        deque.add("cc");

        String[] expected = {"aa", "bb", "cc"};
        String[] actual = new String[3];
        actual[0] = deque.take();
        actual[1] = deque.take();
        actual[2] = deque.take();

        assertThat(actual, is(expected));
        assertThat(deque.size(), is(0));
    }

    @Test
    public void testAddLastAndTakeLast(){
        MyDeque<String> deque = new MyDeque<>();
        deque.addFirst("aa");
        deque.addFirst("bb");
        deque.addFirst("cc");

        String[] expected = {"aa", "bb", "cc"};
        String[] actual = new String[3];
        actual[0] = deque.takeLast();
        actual[1] = deque.takeLast();
        actual[2] = deque.takeLast();

        assertThat(actual, is(expected));
        assertThat(deque.size(), is(0));
    }

    @Test
    public void testAddLastAndTake(){
        MyDeque<String> deque = new MyDeque<>();
        deque.addFirst("aa");
        deque.addFirst("bb");
        deque.addFirst("cc");

        String[] expected = {"cc", "bb", "aa"};
        String[] actual = new String[3];
        actual[0] = deque.take();
        actual[1] = deque.take();
        actual[2] = deque.take();

        assertThat(actual, is(expected));
        assertThat(deque.size(), is(0));
    }
}