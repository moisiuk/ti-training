import org.junit.Test;

import static org.junit.Assert.*;

public class MergeSortTest {
    private MergeSort mergeSort = new MergeSort();

    @Test
    public void testMergeSort() {
        int[] originalArray = {2, 3, 6, 3, 9, 9, 10, 0};
        mergeSort.sort(originalArray);
        int[] expectedArray = {0, 2, 3, 3, 6, 9, 9, 10};
        assertArrayEquals(expectedArray, originalArray);
    }
}

