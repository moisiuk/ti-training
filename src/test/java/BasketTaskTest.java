import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class BasketTaskTest {

    @Test
    public void testIfAmountEqualsSumOfThreeColors() {
        Basket[] actualArray = new Basket[10];
        actualArray[0] = new Basket(Color.WHITE);
        actualArray[1] = new Basket(Color.RED);
        actualArray[2] = new Basket(Color.GREEN);
        actualArray[3] = new Basket(Color.GREEN);
        actualArray[4] = new Basket(Color.WHITE);
        actualArray[5] = new Basket(Color.RED);
        actualArray[6] = new Basket(Color.WHITE);
        actualArray[7] = new Basket(Color.RED);
        actualArray[8] = new Basket(Color.WHITE);
        actualArray[9] = new Basket(Color.GREEN);

        Basket[] expectedArray = new Basket[10];
        expectedArray[0] = new Basket(Color.RED);
        expectedArray[1] = new Basket(Color.RED);
        expectedArray[2] = new Basket(Color.RED);
        expectedArray[3] = new Basket(Color.GREEN);
        expectedArray[4] = new Basket(Color.GREEN);
        expectedArray[5] = new Basket(Color.GREEN);
        expectedArray[6] = new Basket(Color.WHITE);
        expectedArray[7] = new Basket(Color.WHITE);
        expectedArray[8] = new Basket(Color.WHITE);
        expectedArray[9] = new Basket(Color.WHITE);


        BasketTask basketTask = new BasketTask();
        basketTask.sort(actualArray);
        assertThat(actualArray, is(expectedArray));


        Basket[] actualArray1 = new Basket[10];
        actualArray1[0] = new Basket(Color.WHITE);
        actualArray1[1] = new Basket(Color.WHITE);
        actualArray1[2] = new Basket(Color.WHITE);
        actualArray1[3] = new Basket(Color.GREEN);
        actualArray1[4] = new Basket(Color.GREEN);
        actualArray1[5] = new Basket(Color.GREEN);
        actualArray1[6] = new Basket(Color.GREEN);
        actualArray1[7] = new Basket(Color.RED);
        actualArray1[8] = new Basket(Color.RED);
        actualArray1[9] = new Basket(Color.RED);

        Basket[] expectedArray1 = new Basket[10];
        expectedArray1[0] = new Basket(Color.RED);
        expectedArray1[1] = new Basket(Color.RED);
        expectedArray1[2] = new Basket(Color.RED);
        expectedArray1[3] = new Basket(Color.GREEN);
        expectedArray1[4] = new Basket(Color.GREEN);
        expectedArray1[5] = new Basket(Color.GREEN);
        expectedArray1[6] = new Basket(Color.GREEN);
        expectedArray1[7] = new Basket(Color.WHITE);
        expectedArray1[8] = new Basket(Color.WHITE);
        expectedArray1[9] = new Basket(Color.WHITE);

        basketTask.sort(actualArray1);
        assertThat(actualArray1, is(expectedArray1));

        Basket[] actualArray2 = new Basket[14];
        actualArray2[0] = new Basket(Color.WHITE);
        actualArray2[1] = new Basket(Color.WHITE);
        actualArray2[2] = new Basket(Color.WHITE);
        actualArray2[3] = new Basket(Color.OTHER);
        actualArray2[4] = new Basket(Color.GREEN);
        actualArray2[5] = new Basket(Color.GREEN);
        actualArray2[6] = new Basket(Color.OTHER);
        actualArray2[7] = new Basket(Color.OTHER);
        actualArray2[8] = new Basket(Color.GREEN);
        actualArray2[9] = new Basket(Color.GREEN);
        actualArray2[10] = new Basket(Color.RED);
        actualArray2[11] = new Basket(Color.RED);
        actualArray2[12] = new Basket(Color.OTHER);
        actualArray2[13] = new Basket(Color.RED);

        Basket[] expectedArray2 = new Basket[14];
        expectedArray2[0] = new Basket(Color.RED);
        expectedArray2[1] = new Basket(Color.RED);
        expectedArray2[2] = new Basket(Color.RED);
        expectedArray2[3] = new Basket(Color.GREEN);
        expectedArray2[4] = new Basket(Color.GREEN);
        expectedArray2[5] = new Basket(Color.GREEN);
        expectedArray2[6] = new Basket(Color.GREEN);
        expectedArray2[7] = new Basket(Color.WHITE);
        expectedArray2[8] = new Basket(Color.WHITE);
        expectedArray2[9] = new Basket(Color.WHITE);
        expectedArray2[10] = new Basket(Color.OTHER);
        expectedArray2[11] = new Basket(Color.OTHER);
        expectedArray2[12] = new Basket(Color.OTHER);
        expectedArray2[13] = new Basket(Color.OTHER);

        basketTask.sort(actualArray2);
        assertThat(actualArray2, is(expectedArray2));
    }
}